import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AthleteService } from "../../global/athlete/athlete.service";

@Component({
  selector: "athlete",
  templateUrl: "pages/athlete/athlete.html",
  styleUrls: ["pages/athlete/athlete-common.css", "pages/athlete/athlete.css"],
  providers: [AthleteService]
})
export class AthleteComponent implements OnInit {
  athleteName: string;
  athleteProfileImg: string;
  athleteLocation: string;
  athleteTeams: Array<any>;
  athleteCoaches: Array<any>;
  isLoading = false;
  athleteLoaded = false;

  constructor(
  private route: ActivatedRoute,
  private router: Router,
  private athleteService: AthleteService) {}

  ngOnInit() {
    this.isLoading = false;
    this.route.params.forEach((params: Params) => {
      let user = params['username'];
      this.athleteService.getAthlete(user)
        .subscribe(res => {
          this.athleteLoaded = true;
          this.athleteName = res.fullName;
          this.athleteProfileImg = res.profileImg;
          this.athleteLocation = res.location;
          this.athleteTeams = res.teams;
          this.athleteCoaches = res.coaches;
        })
    });
  }

}