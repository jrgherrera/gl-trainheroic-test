import { Component, OnInit} from "@angular/core";
import { Router } from "@angular/router";

import { Athlete } from "../../global/athlete/athlete";
import { AthleteService } from "../../global/athlete/athlete.service";

@Component({
  selector: "list",
  templateUrl: "pages/list/list.html",
  styleUrls: ["pages/list/list-common.css", "pages/list/list.css"],
  providers: [AthleteService]
})
export class ListComponent implements OnInit {
  athletesList: Array<Athlete> = [];
  isLoading = false;
  listLoaded = false;

  constructor(
    private athleteService: AthleteService,
    private router: Router
  ) {}

  ngOnInit() {
    this.isLoading = true;
    this.athleteService.load()
      .subscribe(loadedAthletes => {
        loadedAthletes.results.map(athlete => {
          this.athletesList.push(athlete);
        });
        this.isLoading = false;
        this.listLoaded = true;
      });
  }

  detail (username:string) {
    this.router.navigate([`/athlete/${username}`]);
  }
}