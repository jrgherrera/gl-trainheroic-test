import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { User } from "./user";
import { Config } from "../config";

@Injectable()
export class UserService {
  constructor(private http: Http) {}

  login(user: User) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    
    return this.http.post(`${Config.apiUrl}auth?email=${user.email}&password=${user.password}`, {})
    .map(response => response.json())
    .do(data => {
      Config.token = data.session_id;
    })
    .catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    console.log('error');
    console.log(JSON.stringify(error));
    return Observable.throw(error);
  }
}