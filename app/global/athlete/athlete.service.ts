import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";

import { Config } from "../config";

@Injectable()
export class AthleteService {
  constructor ( private http:Http ) {

  }

  load () {
    return this.http.get(`${Config.apiUrl}public/leaderboard/468425`, {})
    .map(response => {
      return response.json()
    })
    .catch(this.handleErrors);
  }

  getAthlete (user:string) {
    let headers = new Headers();
    headers.append("Session-Token", Config.token);
    return this.http.get(`${Config.apiUrl}1.0/athlete/profile?username=${user}`, {headers: headers})
      .map(response => {
        return response.json();
      })
      .catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    return Observable.throw(error);
  }
}