export class Athlete {
  userFirstName: string;
  userFirstInitial: string;
  userLastName: string;
  userLastInitial: string;
  username: string;
  profileImg: string;
  profileUrl: string;
  userId: number;
  saved_workout_date: any;
  notes: string;
  rating: boolean;
  rank: number;
  avgRank: number;
  tests: Array<string>;
  userTests: Array<any>;
  ranks: Array<number>;
  initials: string;
}