import { LoginComponent } from "./pages/login/login.component";
import { ListComponent } from "./pages/list/list.component";
import { AthleteComponent } from "./pages/athlete/athlete.component";

export const routes = [
  { path: "", component: LoginComponent },
  { path: "list", component: ListComponent },
  { path: "athlete/:username", component: AthleteComponent }
];

export const navigatableComponents = [
  LoginComponent,
  ListComponent,
  AthleteComponent
];
