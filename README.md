# TrainHeroic

This application was developed in Nativescript.
It was the first time working with this framework.
I gave more attention to ios, since the simulator its easier to run, but the project can run in botn platforms.
I used a NativeScript base project and modified it, that is why some images don't match the brand.
Finally, NativeScript is a good tool, but it still need more and better documentation and community.
NG2 made it easier to develop.  

****Requirements:****
- NodeJs 5.6 or higher
- Xcode
- iOS Simulator or a real device
- Android emulator or a real device
- Nativescript cli

****Run the application:****
- Clone this repo and go to develop branch
- Run `npm install`
- Add platforms `tns platform add ios` and `tns platform add android`
- To run ios `tns run ios`
- To run android `tns run android`